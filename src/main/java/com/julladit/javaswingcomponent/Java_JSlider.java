/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author acer
 */
public class Java_JSlider extends JFrame {

    public Java_JSlider() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        Java_JSlider frame = new Java_JSlider();
        frame.pack();
        frame.setVisible(true);
    }
}
