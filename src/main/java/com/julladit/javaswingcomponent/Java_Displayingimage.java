/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.javaswingcomponent;

import java.awt.*;
import javax.swing.JFrame;

/**
 *
 * @author acer
 */
public class Java_Displayingimage extends Canvas {

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("C:\\Users\\acer\\Pictures\\p3.gif");
        g.drawImage(i, 120, 100, this);

    }

    public static void main(String[] args) {
        Java_Displayingimage m = new Java_Displayingimage();
        JFrame f = new JFrame();
        f.add(m);
        f.setSize(400, 400);
        f.setVisible(true);
    }

}
