/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author acer
 */
public class Java_JComboBox {

    JFrame f;

    Java_JComboBox() {
        f = new JFrame("ComboBox");
        String country[] = {"India", "Aus", "U.S.A", "England", "Newzealand"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(50, 50, 90, 20);
        f.add(cb);
        f.setLayout(null);
        f.setSize(400, 500);
        f.setVisible(true);

    }

    public static void main(String[] args) {
        new Java_JComboBox();

    }
}
