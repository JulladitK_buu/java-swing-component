/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author acer
 */
public class Java_JSlide_painting_ticks extends JFrame {

    public Java_JSlide_painting_ticks() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        slider.setMinorTickSpacing(2);
        slider.setMajorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        Java_JSlide_painting_ticks frame = new Java_JSlide_painting_ticks();
        frame.pack();
        frame.setVisible(true);
    }
}
